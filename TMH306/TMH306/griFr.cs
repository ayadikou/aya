﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TMH306
{
    public partial class griFr : Form
    {
        Bitmap kaynak, islem;
        public griFr()
        {
            InitializeComponent();
        }

        private void ortalamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color piksel = kaynak.GetPixel(x, y);
                    int griDeger = (piksel.R + piksel.G + piksel.B) / 3;
                    Color griPiksel = Color.FromArgb(griDeger, griDeger, griDeger);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;
        }

        private void bt709ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renkliPiksel = kaynak.GetPixel(x, y);
                    double griDeger = renkliPiksel.R * 0.2125 + renkliPiksel.G * 0.7154 + renkliPiksel.B * 0.072;
                    int gri = Convert.ToInt32(griDeger);
                    Color yeni = Color.FromArgb(gri, gri, gri);
                    islem.SetPixel(x, y, yeni);
                }
            }

            islemBox.Image = islem;
        }

        private void lumaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen1 = kaynak.Width;
            int yuk1 = kaynak.Height;
            islem = new Bitmap(gen1, yuk1);

            for (int x = 0; x < gen1; x++)
            {
                for (int y = 0; y < yuk1; y++)
                {
                    Color renkliPiksel = kaynak.GetPixel(x, y);
                    double griDeger = renkliPiksel.R * 0.3 + renkliPiksel.G * 0.59 + renkliPiksel.B * 0.11;
                    int gri = Convert.ToInt32(griDeger);
                    Color yenicolor = Color.FromArgb(gri, gri, gri);
                    islem.SetPixel(x, y, yenicolor);
                }
            }

            islemBox.Image = islem;
        }

        private void tekrenkkanalıToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen3 = kaynak.Width;
            int yuk3 = kaynak.Height;
            islem = new Bitmap(gen3, yuk3);

            for (int x = 0; x < gen3; x++)
            {
                for (int y = 0; y < yuk3; y++)
                {
                    Color renkliPiksel = kaynak.GetPixel(x, y);

                    Color griPiksel = Color.FromArgb(renkliPiksel.G, renkliPiksel.G, renkliPiksel.G);

                    islem.SetPixel(x, y, griPiksel);
                }
            }

            islemBox.Image = islem;

        }

        private void açıklıkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;            int yuk = kaynak.Height;            islem = new Bitmap(gen, yuk);            for (int y = 0; y < yuk; y++)            {                for (int x = 0; x < gen; x++)                {                    Color renk = kaynak.GetPixel(x, y);                    int[] dizi = { renk.R, renk.G, renk.B };                    int gri = (dizi.Min() + dizi.Max()) / 2;                    Color griRenk = Color.FromArgb(gri, gri, gri);                    islem.SetPixel(x, y, griRenk);                }            }            islemBox.Image = islem;

        }

        private void normalızeedılmıskanalıToolStripMenuItem_Click(object sender, EventArgs e)
        {

            int gen1 = kaynak.Width;
            int yuk1 = kaynak.Height;
            islem = new Bitmap(gen1, yuk1);


            for (int x = 0; x < gen1; x++)
            {
                for (int y = 0; y < yuk1; y++)
                {
                    Color renkliPiksel = kaynak.GetPixel(x, y);
                    int T = renkliPiksel.R + renkliPiksel.G + renkliPiksel.B;
                    if (T > 0)
                    {

                        int IR = (255 * renkliPiksel.R) / T;

                    }
                    ;
                    Color gri = Color.FromArgb(renkliPiksel.R, renkliPiksel.R, renkliPiksel.R);
                    islem.SetPixel(x, y, gri);

                }
                islemBox.Image = islem;
            }
        }

        private void açToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                kaynak = new Bitmap(openFileDialog1.FileName);
                kaynakBox.Image = kaynak;
            }
        }
    }
}
